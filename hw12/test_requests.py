import requests
import json

BASE_URL = "http://127.0.0.1:5000"
RESULTS_FILE = 'results.txt'

def log_results(response, description):
    result = f'{description}:{response.status_code}\n{response.text}\n'
    with open(RESULTS_FILE, 'a') as file:
        file.write(result)
    print(result)

def test():
    # 1
    response = requests.get(f"{BASE_URL}/students")
    log_results(response, "Get all students")

    # 2
    students_data = [
        {"first_name": "Alice", "last_name": "Wonderland", "age": "21"},
        {"first_name": "Bob", "last_name": "Builder", "age": "22"},
        {"first_name": "Charlie", "last_name": "Brown", "age": "23"}
    ]
    for student in students_data:
        response = requests.post(f"{BASE_URL}/student/create", json=student)
        log_results(response, "Create student")

    # 3
    response = requests.get(f"{BASE_URL}/students")
    log_results(response, "Get all students after creation")

    # 4
    student_id = 2
    update_data = {"age": "24"}
    response = requests.patch(f"{BASE_URL}/student/update/age/{student_id}", json=update_data)
    log_results(response, f"Update age of student with ID {student_id}")

    # 5.
    response = requests.get(f"{BASE_URL}/student/{student_id}")
    log_results(response, f"Get information about student with ID {student_id}")

    # 6.
    student_id = 3
    update_data = {"first_name": "Charles", "last_name": "West", "age": "25"}
    response = requests.put(f"{BASE_URL}/student/update/{student_id}", json=update_data)
    log_results(response, f"Update student with ID {student_id}")

    # 7.
    response = requests.get(f"{BASE_URL}/student/{student_id}")
    log_results(response, f"Get information about student with ID {student_id}")

    # 8.
    response = requests.get(f"{BASE_URL}/students")
    log_results(response, "Get all students before deletion")

    # 9. 
    student_id = 1
    response = requests.delete(f"{BASE_URL}/student/delete/{student_id}")
    log_results(response, f"Delete student with ID {student_id}")

    # 10. 
    response = requests.get(f"{BASE_URL}/students")
    log_results(response, "Get all students after deletion")

if __name__ == "__main__":
    test()
