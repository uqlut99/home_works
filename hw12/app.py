from flask import Flask, jsonify, request
import csv
import os

app = Flask(__name__)
CSV_FILE = 'students.csv'
ERR_MSG = 'Something went wrong'


def read_students():
    students = []
    try:
        if os.path.exists(CSV_FILE):

            with open(CSV_FILE, mode='r') as file:
                reader = csv.DictReader(file)
                for row in reader:
                    students.append(row)
        return students
    except Exception as e:
        print(e)


def append_students(students):
    file_exists = os.path.isfile(CSV_FILE)
    try:
        with open(CSV_FILE, mode='a', newline='') as file:
            fieldnames = ['id', 'first_name', 'last_name', 'age']
            writer = csv.DictWriter(file, fieldnames=fieldnames)

            if not file_exists or os.path.getsize(CSV_FILE) == 0:
                writer.writeheader()

            for student in students:
                writer.writerow(student)
    except Exception as e:
        print(e)


def write_students(students):
    try:
        with open(CSV_FILE, mode='w', newline='') as file:
            fieldnames = ['id', 'first_name', 'last_name', 'age']
            writer = csv.DictWriter(file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(students)
    except Exception as e:
        print(e)


class StudentsApi:

    def __init__(self, app):
        self.app = app
        self.routes()

    def routes(self):
        self.app.add_url_rule('/students', 'get_all_students', self.get_all_students, methods=['GET'])
        self.app.add_url_rule('/student/<int:id>', 'get_student_by_id', self.get_student_by_id, methods=['GET'])
        self.app.add_url_rule('/student/<string:surname>', 'get_stundent_by_surname', self.get_student_by_surname,
                              methods=['GET'])
        self.app.add_url_rule('/student/create', 'create_student', self.create_student, methods=['POST'])
        self.app.add_url_rule('/student/update/<int:id>', 'update_student_by_id', self.update_student_by_id,
                              methods=['PUT'])
        self.app.add_url_rule('/student/update/age/<int:id>', 'update_age_student_by_id', self.update_age_student_by_id,
                              methods=['PATCH'])
        self.app.add_url_rule('/student/delete/<int:id>', 'delete_student', self.delete_student,
                              methods=['DELETE'])

    def get_all_students(self):
        try:
            students = read_students()
            return students
        except Exception as e:
            print(e)
            return ERR_MSG

    def get_student_by_id(self, id):
        try:
            students = read_students()
            for student in students:
                if student['id'] == str(id):
                    jsonify(student)
            return f'We cant find student with id - {id} '
        except Exception as e:
            print(e)
            return ERR_MSG

    def get_student_by_surname(self, last_name):
        try:
            student_lst = []
            students = read_students()
            for student in students:
                if student['last_name'] == last_name:
                    student_lst.append(student)
            if student_lst:
                return jsonify(student_lst)
            return f'Cant find students with second name - {last_name}'
        except Exception as e:
            print(e)
            return ERR_MSG

    def create_student(self):
        try:
            students = read_students()
            student_last_id = int(students[-1]['id']) + 1
            student = request.get_json()
            if student['first_name'] != '' or student['second_name'] != '' or student['age'] != '':
                new_student = {
                    'id': f'{student_last_id}',
                    'first_name': student['first_name'],
                    'last_name': student['last_name'],
                    'age': student['age']
                }
                append_students([new_student])
                return read_students()[-1]
            return ERR_MSG
        except Exception as e:
            print(e)
            return ERR_MSG

    def update_student_by_id(self, id):
        try:
            students = read_students()
            student_new_data = request.get_json()
            updated = False
            for student in students:
                if str(id) == student['id']:
                    student['first_name'] = student_new_data.get('first_name', student['first_name'])
                    student['last_name'] = student_new_data.get('last_name', student['last_name'])
                    student['age'] = student_new_data.get('age', student['age'])
                    updated = True
            if not updated:
                f'We cant find student with id - {id} '
            write_students(students)
            return jsonify(student), 200

        except Exception as e:
            print(e)
            return ERR_MSG

    def update_age_student_by_id(self, id):
        try:
            students = read_students()
            student_new_data = request.get_json()
            updated = False
            for student in students:
                if str(id) == student['id']:
                    student['age'] = student_new_data.get('age', student['age'])
                    updated = True
            if not updated:
                return f'We cant find student with id - {id} '
            write_students(students)
            return jsonify(student), 200
        except Exception as e:
            print(e)
            return ERR_MSG

    def delete_student(self, id):
        try:
            students = read_students()
            for student in students:
                if str(id) == student['id']:
                    students.pop(students.index(student))
                    write_students(students)
                    return f'Student has been deleted - {student}'
            return f'We cant find student with id - {id} '
        except Exception as e:
            print(e)
            return ERR_MSG


user_api = StudentsApi(app)

if __name__ == '__main__':

    app.run(debug=True, use_reloader=False)

