resource "aws_vpc" "vpc_hw19" {
    cidr_block       = "10.0.0.0/16"

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_subnet" "public_subnet_hw19" {
    vpc_id     = aws_vpc.vpc_hw19.id
    cidr_block = "10.0.1.0/24"

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_subnet" "private_subnet_hw19" {
    vpc_id     = aws_vpc.vpc_hw19.id
    cidr_block = "10.0.10.0/24"

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_internet_gateway" "internet_gateway_hw19" {
    vpc_id = aws_vpc.vpc_hw19.id

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_route_table" "public_routetable_hw19" {
    vpc_id = aws_vpc.vpc_hw19.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.internet_gateway_hw19.id
    }

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_route_table_association" "public_routetable_association_hw19" {
    subnet_id      = aws_subnet.public_subnet_hw19.id
    route_table_id = aws_route_table.public_routetable_hw19.id
}

resource "aws_route_table" "private_routetable_hw19" {
    vpc_id = aws_vpc.vpc_hw19.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.nat_gateway_hw19.id
    }

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_route_table_association" "private_routetable_association_hw19" {
    subnet_id      = aws_subnet.private_subnet_hw19.id
    route_table_id = aws_route_table.private_routetable_hw19.id
}

resource "aws_instance" "public_ec2_hw19" {
    ami                   = "ami-04f76ebf53292ef4d"
    instance_type         = "t2.micro"
    associate_public_ip_address = true
    key_name              = "ssh"
    subnet_id             = aws_subnet.public_subnet_hw19.id
    vpc_security_group_ids = [aws_security_group.public_sg_hw19.id]

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_instance" "private_ec2_hw19" {
    ami                   = "ami-04f76ebf53292ef4d"
    instance_type         = "t2.micro"
    key_name              = "ssh"
    subnet_id             = aws_subnet.private_subnet_hw19.id
    vpc_security_group_ids = [aws_security_group.private_sg_hw19.id]

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_eip" "nat_gateway" {
    vpc = true
}

resource "aws_nat_gateway" "nat_gateway_hw19" {
    allocation_id = aws_eip.nat_gateway.id
    subnet_id     = aws_subnet.public_subnet_hw19.id

    tags = {    
        Name = "${var.general_tag["Name"]}"
    }
}

resource "aws_security_group" "public_sg_hw19" {
    vpc_id = aws_vpc.vpc_hw19.id

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}


resource "aws_security_group" "private_sg_hw19" {
    vpc_id = aws_vpc.vpc_hw19.id

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        security_groups = [aws_security_group.public_sg_hw19.id]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}
