variable "general_tag" {
    description = "General tag"
    type = map(any)
    default = {
      Name = "homework19"
    }
}