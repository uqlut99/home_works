resource "aws_instance" "ec2_hw20" {
    ami                   = "ami-04cdc91e49cb06165"
    instance_type         = "t3.micro"
    associate_public_ip_address = true
    key_name              = "ssh"
    vpc_security_group_ids = [aws_security_group.allow_http_https.id]
    user_data = file("${path.module}/user_data/script.sh")
    tags = {
        Name = "${var.general_tag["Name"]}"
    }
}
