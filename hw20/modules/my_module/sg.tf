resource "aws_security_group" "allow_http_https" {
  name        = "allow-http-https"
  description = "Allow, HTTP, HTTPS"
  vpc_id      = var.vpc_id

  tags = {
    Name = var.general_tag["Name"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = var.list_of_open_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}