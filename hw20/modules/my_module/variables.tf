variable "vpc_id" {
    description = "The VPC id"
    type = string
}

variable "list_of_open_ports" {
    description = "The list of open ports"
    type = list(number)
}

variable "general_tag" {
    description = "The dictionary of tags"
    type = map(any)
    default = {
        "Name": "Homework_20"
    }
}