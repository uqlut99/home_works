terraform {
  backend "s3" {
    bucket = "hw20-bucket-danit"
    key = "users/uqlut99/terraform.tfstate"
    region = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}