#!/bin/bash
disk_mem=$(df | grep vda1 | awk '{print $5}' | tr -d '%')
echo "Disk memory used - ${disk_mem}%"
if [[ disk_mem < $1 ]]; then
	echo "Disk memory used - ${disk_mem}%."
else
	echo "WARNING. YOU HAVE LESS THEN $1 % MEMORY ON YOUR DISK" >> /var/log/disk.log
fi 
