#!/bin/bash
set -x
PASSWORD=123321

groupadd developers
groupadd webmasters

arr_developers=("dev1" "dev2" "dev3")

for dev in ${arr_developers[@]}; do
#	read -p "Write password for $dev - " PASSWORD
	useradd -m -s /bin/bash -G developers -p $PASSWORD $dev
done
usermod -aG webmasters dev1
useradd -m -s /bin/bash -p $PASSWORD backupdev
touch /home/dev1/file_to_clone # Testing file
rm -rfv /home/backupdev/.* # Clear everything in /home/backupdev so after we can clone dev1 homepath
cp -r /home/dev1/. /home/backupdev # Clone dev1 to backupdev
mkdir /home/web_project
chmod g+rw /home/web_project
chown root:developers /home/web_project
touch /home/my.log
chattr +a /home/my.log
