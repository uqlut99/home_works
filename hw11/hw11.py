english_alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                    'u', 'v', 'w', 'x', 'y', 'z']


def create_str_of_alphabet(lst_alphabet) -> str:
    return "".join(lst_alphabet)


class Alphabet:

    def __init__(self, lang: str, letters: list):
        self.lang = lang
        self.letters = letters

    def print_alphabet(self):
        for letter in self.letters:
            print(letter, end=' ')

    def letters_num(self) -> int:
        return len(self.letters)


class EngAlphabet(Alphabet):

    _letters_num = len(english_alphabet)

    def __init__(self, lang: str, letters: list, lang_designation: str, str_of_alphabet: str):
        super().__init__(lang, letters)
        self.lang_designation = lang_designation
        self.str_of_alphabet = str_of_alphabet

    def is_en_letter(self, letter: str):
        letter = letter.lower()
        if letter in self.letters:
            print(f'{letter} - English letter')
        else:
            print(f'{letter} - not English letter')

    def letters_num(self) -> int:
        return self._letters_num

    @staticmethod
    def example() -> str:
        return "Hello, this is example test. Nice to meet you. See you later!"


def main():
    eng_object = EngAlphabet(lang="English", letters=english_alphabet, lang_designation='EN',
                             str_of_alphabet=create_str_of_alphabet(english_alphabet))

    eng_object.print_alphabet()
    print(eng_object.letters_num())
    eng_object.is_en_letter("F")
    eng_object.is_en_letter("Щ")
    print(eng_object.example())


main()