#!/bin/bash
#set -x
RANDOM_NUMBER=$((1 + RANDOM % 100))
not_found=true
for ((i = 0; i < 5; i++)) ; do
	echo "${RANDOM_NUMBER}"
	read -p "Guess the number from 1 to 100 - " number
	if [[ ${number} < ${RANDOM_NUMBER} ]] ; then
		echo "${number} is lower then random number"
	elif [[ ${number} > ${RANDOM_NUMBER} ]] ; then
		echo "${number} is higher then random number"
	else
		echo "You are correct"
		echo "The number was - ${RANDOM_NUMBER}"
		not_found=false
		break
	fi
done
if [[ "${not_found}" = true ]]; then
	echo "You are lost. The number was - ${RANDOM_NUMBER}"
fi


