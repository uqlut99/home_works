resource "aws_instance" "ec2_hw21" {
    ami                   = "ami-00f07845aed8c0ee7"
    instance_type         = "t2.micro"
    associate_public_ip_address = true
    key_name              = "ssh"
    user_data = file("${path.module}/userdata.sh")
    vpc_security_group_ids = [ aws_security_group.sg_allow_ssh.id, aws_security_group.sg_allow_http_https.id ]
    count = 2
    tags = {
        Name = "${var.tag["Name"]}"
    }
}

resource "aws_security_group" "sg_allow_ssh" {
    description = "Allow SSH"
    vpc_id      = data.aws_vpc.default.id

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "sg_allow_http_https" {
  description = "Allow HTTP and HTTPS"
  vpc_id      = data.aws_vpc.default.id
  dynamic "ingress" {
    for_each = [80, 443]
    content {
      from_port        = ingress.value
      to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  
}

output "ansible_inventory" {
  value = templatefile("${path.module}/inventory.tpl", {
    instances = aws_instance.ec2_hw21
  })
}

resource "local_file" "inventory" {
  content  = templatefile("${path.module}/inventory.tpl", {
    instances = aws_instance.ec2_hw21
  })
  filename = "${path.module}/inventory.ini"
}


data "aws_vpc" "default" {
  default = true
}
  