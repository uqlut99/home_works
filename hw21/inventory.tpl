[ec2_instances]
%{ for instance in instances }
${instance.public_ip} ansible_user=ec2-user ansible_ssh_private_key_file=/root/hw21_test/ssh.pem
%{ endfor }