variable "tag" {
    description = "Tag for HW21"
    type = map(any)
    default = {
      Name = "Homework_21"
    }
  
}