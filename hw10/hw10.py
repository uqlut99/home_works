import random


class Game:
    def __init__(self, number: int):
        self.number = number

    def number_guesser(self):
        usr_win = False
        games_number = 0
        for i in range(5):
            games_number += 1
            usr_num = input("Guess the number from 1 to 100: ")
            if int(usr_num) < self.number:
                print("Your number < random number")
            elif int(usr_num) > self.number:
                print("Your number > random number")
            else:
                print('You won')
                usr_win = True
                break
        if not usr_win:
            print('You lost')
        return games_number


game = Game(random.randrange(1, 100))
game.number_guesser()
